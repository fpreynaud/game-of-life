import random
from os import system
from time import sleep
import sys

gridSize = 44
columns = 16*4
rows = 9*4 
aliveCells = 0

emojiMode = True
if emojiMode:
	aliveCell = '\U0001F7E9' # green square
	deadCell =  '\U00002B1C' # white square
else:
	aliveCell = '@'
	deadCell = ' '


def display():
	system('clear')
	for i in range(rows):
		for j in range(columns):
			sys.stdout.write('\x1b[47;{1}m{0}\x1b[0m'.format(aliveCell if grid[i][j] == 1 else deadCell, 32))
		sys.stdout.write('\n')
	
def put_glider(position):
	r,c = position
	grid[r%rows][c%columns] = 1
	grid[r%rows][(c+1)%columns] = 1
	grid[r%rows][(c+2)%columns] = 1
	grid[(r+1)%rows][(c+2)%columns] = 1
	grid[(r+2)%rows][(c+1)%columns] = 1


generation = 0
while True:
	grid = []
	#initialAliveCells = random.randrange(gridSize**2)
	initialAliveCells = 64

	for _ in range(rows):
		row = []
		for _ in range(columns):
			row.append(0)
		grid.append(row)

	#Random start
	"""
	for _ in range(initialAliveCells):
		row,column  = random.randrange(rows), random.randrange(columns)
		while grid[row][column] == 1:
			row,column  = random.randrange(rows), random.randrange(columns)
		grid[row][column] = 1
	"""
	

	"""
	grid[7][6] = 1
	grid[8][8] = 1
	grid[7][5] = 1
	grid[8][7] = 1
	grid[8][9] = 1
	grid[10][9] = 1
	grid[8][11] = 1
	grid[6][11] = 1
	"""
	while True:
		display()
		if generation == 0:
			put_glider((random.randrange(rows), random.randrange(columns)))
		generation = (generation + 1) % 10

		aliveCells = 0
		
		for row in range(rows):
			for column in range(columns):

				aliveNeighbors = 0
				for i in range(3):
					for j in range(3):
						if grid[(row-1+i)%rows][(column-1+j)%columns] in (1,2,7) and (i,j) != (1,1):
							aliveNeighbors += 1

				if grid[row][column] == 0:
					grid[row][column] = 5 if aliveNeighbors == 3 else 0
				elif grid[row][column] == 1:
					aliveCells += 1
					grid[row][column] = 7 if aliveNeighbors in (2,3) else 2

		for row in range(rows):
			for column in range(columns):
				grid[row][column] %= 2

		if aliveCells == 0:
			break
		sleep(.1)
		#input()

